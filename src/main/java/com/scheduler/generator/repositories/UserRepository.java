package com.scheduler.generator.repositories;


import com.scheduler.generator.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sam.
 */
public interface UserRepository extends JpaRepository<User, Long>{
    /**
     * возвращает первого найденного пользователя по логину
     *
     * @param login - логин пользователя
     * @return пользователя {@link User}
     */
    public User findFirstByLogin(String login);

}
