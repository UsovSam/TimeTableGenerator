package com.scheduler.generator.services;



import com.scheduler.generator.entities.User;

import java.util.List;

/**
 * Created by Sam.
 */
public interface UserService {


    /**
     * Получение информации о пользователе по id
     * @param id - идентификатор пользователя
     * @return информация о пользователе
     */
    public User getById(Long id);

    /**
     * Сохранение пользователя.
     *
     * @param user пользователь.
     * @return объект пользователя.
     */
    public User save(User user);

    /**
     * Проверка существования пользователя по логину.
     *
     * @param login логин.
     * @return true|false.
     */
    public boolean isUserExist(String login);

    /**
     * Поиск пользователя по логину.
     *
     * @param login логин.
     * @return пользователь.
     */
    public User getByLogin(String login);

    /**
     * Получение всех пользователе за исключением текущего.
     *
     * @param id идентификатор текущего пользователя.
     * @return массив пользователей.
     */
    public List<User> getAllUsers(Long id);

    /**
     * Удаление записи о пользователе из базы данных по идентификатору.
     *
     * @param id идентификатор пользователя.
     */
    public void deleteById(Long id);

    /**
     * Кодирование пароля
     * @param password - пароль
     * @return закодированый пароль
     */
    public String encodePassword(String password);

    /**
     * Проверка соответсвия паролей
     * @param rawPassword - пароль 1
     * @param encodedPassword - пароль 2
     * @return результат соответствия
     */
    public Boolean matchPassword(String rawPassword, String encodedPassword);


}

