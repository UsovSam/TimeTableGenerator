package com.scheduler.generator.services.imp;


import com.scheduler.generator.repositories.UserRepository;
import com.scheduler.generator.services.UserService;
import com.scheduler.generator.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sam.
 */
@Service
public class UserServiceImp implements UserService {

    private final BCryptPasswordEncoder bcrypt;

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository, BCryptPasswordEncoder bcrypt) {
        this.userRepository = userRepository;
        this.bcrypt = bcrypt;
    }


    @Override
    public User getById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User save(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public boolean isUserExist(String login) {
        return userRepository.findFirstByLogin(login) != null;
    }

    @Override
    public User getByLogin(String login) {
        return userRepository.findFirstByLogin(login);
    }

    @Override
    public List<User> getAllUsers(Long id) {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        userRepository.delete(id);
    }

    @Override
    public String encodePassword(String password) {
        return bcrypt.encode(password);
    }

    @Override
    public Boolean matchPassword(String rawPassword, String encodedPassword) {
        return bcrypt.matches(rawPassword, encodedPassword);

    }
}
