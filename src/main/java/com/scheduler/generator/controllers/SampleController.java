package com.scheduler.generator.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Sam.
 */
@Controller
public class SampleController {

    @RequestMapping("/")
    String home() {
        return "index";
    }

    @RequestMapping("/hello")
    @ResponseBody
    String hello() {
        return "index";
    }

    @RequestMapping("/login")
    String login() {
        return "login";
    }

}