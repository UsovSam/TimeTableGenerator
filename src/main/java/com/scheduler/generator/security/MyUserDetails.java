package com.scheduler.generator.security;


import com.scheduler.generator.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Пользователь системы
 */
public class MyUserDetails implements UserDetails {

    private final User user;
    private Collection<GrantedAuthority> authorities;

    public MyUserDetails(final User user, Collection<GrantedAuthority> authorities) {
        this.user = user;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return getUser().getPassword();
    }

    @Override
    public String getUsername() {
        return getUser().getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user != null;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    public Long getUserId() {
        return user.getId();
    }

}
