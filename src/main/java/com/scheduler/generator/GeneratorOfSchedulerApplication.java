package com.scheduler.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class GeneratorOfSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorOfSchedulerApplication.class, args);
	}
}
